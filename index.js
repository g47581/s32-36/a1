const express = require('express');
const mongoose = require('mongoose');
const dotenv = require('dotenv').config();
const cors = require('cors')

const PORT = process.env.PORT || 3007;
const app = express();


// Connect our routes module
const userRoutes = require('./routes/userRoutes');


//Middleware to handle JSON payloads
app.use(express.json())
app.use(express.urlencoded({extended:true}))
app.use(cors())
//prevents blocking of requests from client esp different domains




// Connect Database to server
mongoose.connect(process.env.MONGO_URL, {useNewUrlParser: true, useUnifiedTopology: true});


// Test DB Connection
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => console.log(`Connected to Database`));


// Schema
	//schema is in models module


// Routes
	//create a middleware to be the root url of all routes
app.use(`/api/users`, userRoutes);




app.listen(PORT, () => console.log(`Server connected to port ${PORT}`))